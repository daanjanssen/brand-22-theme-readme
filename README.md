# Brand theme

## Shortcodes

Custom made met een lading shortcodes. Dit zijn ze, met hun variabelen en default waardes. Variabelen hoef je alleen mee te geven als ze afwijken van default.

### Lijst met diensten

`[diensten_lijst show="-1" link="Tekst" featured="false"]`

* **show**: getal — aantal diensten zichtbaar (-1 is alle)
* **link**: tekst — verschijnt op de button (default is Lees meer)
* **featured**: true of false — laat alleen die diensten zijn die featured aangevinkt hebben in de dienst detail page

### Calendly knop en content

`[calendly title="Visuele richting nodig?" text="Plan een gratis Visuele Verkenning in. Een gratis gesprek van 30 minuten waarin wij kijken naar de kansen die Visual Thinking biedt voor jou en jouw bedrijf." button="Plan een Visuele Verkenning" kleur="f6862f" button_color="orange" button_only="false"]`

* **title**: tekst — titel verschijnt bovenaan
* **text**: tekst — in de body onder de titel
* **button**: tekst — verschijnt op de button
* **kleur**: HEX kleurcode — zonder #, voor in het Calendly popupscherm
* **button_color**: orange, blue, inverse — kleur van de button
* **button_only**: true of false — laat alleen een knop zien zonder titel en content

### Laat een slider zien

`[slider id="1" image="" content="" singleslide="" exclude=""]` 

* **id**: getal — post id van de slider
* **image**: true of false — indien ingevuld en false verschijnt er alleen tekst
* **content**: true of false — indien ingevuld en false verschijnt er alleen een image
* **singleslide**: getal — indien ingevuld laat hij alleen slide X zien van de geselecteerde slider
* **exclude**: getal(len) — kommagescheiden, als ingevuld laat hij slides x,y,z niet zien van de geselecteerde slider

### Generieke loop die gespecificeerde items laat zien

`[post_loop show="20" offset="0" type="post" id="1" style="grid" category="" imageonly="" alternate_titles=""]`

* **show**: getal — aantal posts om op te halen
* **offset**: getal — begint posts op te halen vanaf post X
* **type**: post type — post, trainingen, etc
* **id**: getal(len) — kommagescheiden, haalt alleen deze posts op als ingevuld
* **style**: grid of list — list verschijnt onder elkaar, grid naast elkaar
* **category**: slugnaam categorie — haal alleen posts die hieronder vallen op
* **imageonly**: true of false — laat alleen het plaatje zien, plaatje klikbaar
* **alternate_titles**: true of false — laat alternatieve titel zien (alternatieve titel in de WP editor)

### Loop die alle Brandmedewerkers laat zien

`[team_lijst include="" exclude="" vacature="false"]`

* **include**: getal(len) — kommagescheiden, id's om te laten zien
* **exclude**: getal(len) — kommagescheiden, id's om NIET te laten zien
* **vacature**: true of false — laat vacatureblok zien in de lijst

### React applicatie die posts met tag filters laat zien (NB: in principe alleen voor posts en beeldbank)

`[reactposts show="10" type="post"]`

* **show**: getal — aantal posts per 'pagina' (voor de read more knop)
* **type**: post-type — haalt posts van dit type op

### Mailerlite formulier

`[mailerlite color="black"]`

* **color**: black / white — bepaalt tekstkleur header en alinea

### En verder

* `[timeline]` — De Brand tijdlijn
* `[vacatures]` — Lijst me alle vacatures
* `[trainingspad]` — Laat gekoppelde trainingen in een trainingspad zien
* `[socials]` — Social media icons met links zoals gedefinieerd in Theme Options -> Socials
* `[brand_language_switch]` — Laat de language switcher zien


### Build commands

* `composer` — Install composer deps
* `yarn start` — Compile assets when file changes are made, start Browsersync session
* `yarn build` — Compile and optimize the files in your assets directory
* `yarn build:production` — Compile assets for production